module rabbitmq

go 1.13

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
)
