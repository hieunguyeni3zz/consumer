package main

import (
	"rabbitmq/consumer"
	"rabbitmq/pub"
)

func main() {
	pub.RunPublisher()
	consumer.RunConsumer()
}
