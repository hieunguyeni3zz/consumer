package utils

const (
	TOPIC_UPDATE_EMAIL_ORDER_RECENT = "finan-order:update_email_order_recent"
	TOPIC_SEND_EMAIL_ORDER = "finan-order:send-email-order"
	TOPIC_SET_USER_GUIDE       = "ms-meta-data:topic_set_user_guide"
	TOPIC_UPDATE_DEBT_AMOUNT = "ms-business-management:update_contact_debt_amount"
)

const (
	FINAN_ORDER = "http://localhost:8000"
	RABBIT_MQ = ":8011"
	MS_META_DATA = "http://localhost:8082"
	MS_BUSINESS_MANAGEMENT = "http://localhost:8012"
	MS_PRODUCT_MANAGEMENT = "http://localhost:8094"
)
